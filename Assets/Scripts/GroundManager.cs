using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundManager : MonoBehaviour
{
    public Transform[] groundObjects;

    void Update()
    {
        if (!GameManager.Instance.isGameOver && GameManager.Instance.hasStarted)
        {
            Vector3 displacement = GameManager.Instance.gameSpeed * Vector3.left * Time.deltaTime;
            foreach (Transform ground in groundObjects)
            {
                ground.Translate(displacement);
            }
        }
    }
}
