using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorController : MonoBehaviour
{
    public BirdController player;

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Pipe"))
        {
            player.AddPoint();
        }
    }
}
