using UnityEngine;

public class GroundController : MonoBehaviour
{
    public Transform wrapToObject;

    private new SpriteRenderer renderer;

    void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (!renderer.isVisible && transform.position.x < 0)
        {
            transform.position = new Vector3(wrapToObject.position.x + renderer.bounds.size.x, transform.position.y);
        }
    }
}
