using UnityEngine;

public class CollidableController : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D other)
    {
        BirdController player = other.gameObject.GetComponent<BirdController>();

        if(player)
        {
            player.Die();
            GameManager.Instance.GameOver();
        }
    }
}
