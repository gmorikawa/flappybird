using UnityEngine;
using UnityEngine.UI;

public class BirdController : MonoBehaviour
{
    [Range(1, 5)]
    public float jumpForce;

    public Text scoreUI;

    private Rigidbody2D body;
    private Animator animator;
    private Vector2 jumpVector;
    private int points;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        jumpVector = new Vector2(0f, 100f);
        points = 0;
    }

    void Update()
    {
        if(!GameManager.Instance.isGameOver)
        {
            if (Input.GetMouseButtonDown(0))
            {
                animator.SetTrigger("FlyUp");
                body.velocity = Vector2.zero;
                body.AddForce(jumpVector * jumpForce);
            }
        }
    }

    public void Die()
    {
        if (!GameManager.Instance.isGameOver)
        {
            body.velocity = Vector2.zero;
            body.AddForce(new Vector2(50f, 200f));
            body.AddTorque(300f);
        }
    }

    public void AddPoint()
    {
        points++;
        scoreUI.text = $"Score: {points}";
    }
}
