using UnityEngine;

public class PipeController : MonoBehaviour
{
    private new SpriteRenderer renderer;

    void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if(!GameManager.Instance.isGameOver)
        {
            transform.Translate(GameManager.Instance.gameSpeed * Vector3.left * Time.deltaTime);
        }

        if(!renderer.isVisible && transform.position.x < 0)
        {
            Destroy(gameObject);
        }
    }
}
