using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // singleton
    private static GameManager _instance = null;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameManager>();
            return _instance;
        }
    }

    [Header("Objects")]
    public PipeManager pipeManager;
    public BirdController player;


    [Header("UI elements")]
    public GameObject gameOverScreen;

    [Header("Flags")]
    public bool isGameOver = false;
    public bool hasStarted = false;

    [Header("Configuration")]
    public float gameSpeed;

    public void GameStart()
    {
        hasStarted = true;
        pipeManager.StartSpawn();
        player.GetComponent<Rigidbody2D>().gravityScale = 2f;

        gameOverScreen.SetActive(false);
    }

    public void GameOver()
    {
        isGameOver = true;
        pipeManager.StopSpawn();

        gameOverScreen.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene("GameScene");
    }
}
