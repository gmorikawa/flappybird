using System.Collections;
using UnityEngine;

public class PipeManager : MonoBehaviour
{
    public GameObject pipePrefab;
    public float minHeightPoint;
    public float maxHeightPoint;
    public float pipeHeightDistance;

    private Vector3 upperSpawnPoint;
    private Vector3 bottomSpawnPoint;
    private float maxPipeHeight;
    private float spawnTimeSeconds;
    private float timeElapsed;

    private IEnumerator spawnerRoutine;

    void Start()
    {
        spawnTimeSeconds = 3f;
        timeElapsed = 0f;

        upperSpawnPoint = new Vector3(transform.position.x, maxHeightPoint);
        bottomSpawnPoint = new Vector3(transform.position.x, minHeightPoint);
        maxPipeHeight = (maxHeightPoint - minHeightPoint) - pipeHeightDistance;
    }

    public void StartSpawn()
    {
        spawnerRoutine = Spawner();
        StartCoroutine(spawnerRoutine);
    }

    public void StopSpawn()
    {
        timeElapsed = 0f;
        StopCoroutine(spawnerRoutine);
    }

    private IEnumerator Spawner()
    {
        while(true)
        {
            timeElapsed += Time.deltaTime;
            if (!GameManager.Instance.isGameOver && timeElapsed >= spawnTimeSeconds)
            {
                SpawnNewSetPipes();
                timeElapsed = 0f;
            }

            yield return null;
        }
    }

    private void SpawnNewSetPipes()
    {
        float pipeHeight = Random.Range(1f, maxPipeHeight);
        GameObject newPipeBottom = CreatePipe(pipePrefab, bottomSpawnPoint, pipeHeight);
        GameObject newPipeUpper = CreatePipe(pipePrefab, upperSpawnPoint, maxPipeHeight - pipeHeight);
        newPipeUpper.GetComponent<SpriteRenderer>().flipY = true;
        newPipeUpper.GetComponent<BoxCollider2D>().offset *= -1;
    }

    private GameObject CreatePipe(GameObject _pipePrefab, Vector3 _spawnLocation, float _height)
    {
        GameObject newPipe = Instantiate(_pipePrefab, _spawnLocation, Quaternion.identity);
        BoxCollider2D pipeCollider = newPipe.GetComponent<BoxCollider2D>();
        SpriteRenderer pipeRenderer = newPipe.GetComponent<SpriteRenderer>();

        pipeRenderer.size = new Vector2(1f, _height);
        pipeCollider.size = new Vector2(0.625f, _height);
        pipeCollider.offset = new Vector2(0, _height / 2f);

        return newPipe;
    }
}
